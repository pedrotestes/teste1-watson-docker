const express = require('express');
const axios = require('axios');
const server = express();
const bodyParser = require('body-parser');
require('dotenv').config();
PORT = process.env.PORT || 8081;
const AssistantV1 = require('ibm-watson/assistant/v1');
const { IamAuthenticator } = require('ibm-watson/auth');
const assistant = new AssistantV1({
  authenticator: new IamAuthenticator({ apikey: 'cw5OoN6U8ylPLk0jzIjAGriWqZJOWCIE5QcExkTXuCLZ' }),
  serviceUrl: 'https://api.us-south.assistant.watson.cloud.ibm.com/instances/8da4f8aa-20fa-4c69-a05a-59b2b049af93',
  version: '2018-02-16'
});
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));

server.get('/', (req, res) => {
    res.send('Hello World!')
  })

  
server.post('/watson', (req, res) => {
    console.log(req.body)

    const {input, context} = req.body;
    
    assistant.message(
        {
          input,
          workspaceId: 'e3a01633-31eb-4a78-9490-917a458b8a3d',
          context
        })
        .then(response => {
            res.send(response.result)
        })
        .catch(err => {
          console.log(err);
        });

    })

server.listen(PORT, () => {
    console.log(`ouvindo em http://localhost:${PORT}`)
})


//kkkkkkk

module.exports = server;